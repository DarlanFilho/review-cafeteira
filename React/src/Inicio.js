import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const axios = require('axios');

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
   root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  card: {
    maxWidth: 350,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

class Inicio extends React.Component {

  constructor(props) {

    super(props)
    this.state = {

      reviews:[],
      token:'',

    }
  }

  componentDidMount() {

   axios.get('http://localhost:3001/api/reviews?filter[include][revisor]')
          .then((response) => {
            this.setState({reviews:response.data})
            console.log(response.data)})
          .catch((error) => console.log(error));

    this.setState({token: this.props.match.params.token })
  }
  render() {
    const { classes } = this.props;

    return(

      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <Typography variant="title" color="inherit" className={classes.grow}>
              <Link to="/login"><Button variant="contained" color="primary" className={classes.button}>Login</Button></Link>
            </Typography>
          </Toolbar>
        </AppBar>
        <br />
        <center>
          <Typography variant="display5" component="h2">
            REVIEWS
          </Typography>
          {this.state.reviews.map((item) => {
            return (
              <Card className={classes.card} key={item.date}>
                <CardContent>
                  <Typography variant="display2" component="h2">
                    {item.coffeeShopId}
                  </Typography>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Revisor: {item.revisor.username}
                  </Typography>
                  <Typography className={classes.pos} color="textSecondary">
                    Data: {new Date(item.date).getDate() + "/" + new Date(item.date).getMonth() + "/" + new Date(item.date).getFullYear()}
                  </Typography>
                  <Typography component="p">
                    {item.nota} estrelas
                     <br />
                    {item.comentario}
                  </Typography>
                </CardContent>
              </Card>
            )
          })}
        </center>
      </div>
    )
  }
}
Inicio.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Inicio);