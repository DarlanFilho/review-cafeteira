import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import CardActions from '@material-ui/core/CardActions';

const axios = require('axios');

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
   root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  card: {
    maxWidth: 350,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  actions: {
    display: 'flex',
  },
  delete: {
    marginRight: theme.spacing.unit,
  },
});

class RevisorReview extends React.Component {

  constructor(props) {

    super(props)
    this.state = {

      reviews:[],
      token:'',
      user:'',
      nome:''

    }
  }

  componentDidMount() {

    this.pegarDados();

   axios.get('http://localhost:3001/api/Revisores/' + this.props.match.params.id )
          .then((response) => {
            this.setState({nome:response.data.username})
            console.log(response.data)})
          .catch((error) => console.log(error));

          this.setState({token: this.props.match.params.token, user: this.props.match.params.id })
  }

  handleDelete = id => event => {

    axios.delete('http://localhost:3001/api/Reviews/' + id + '?access_token=' + this.props.match.params.token)
    .then((response) => {
      this.pegarDados();
      console.log(response.data)})
    .catch((error) => console.log(error));
   }

   pegarDados() {

    axios.get('http://localhost:3001/api/Revisores/' + this.props.match.params.id + '/reviews?access_token=' + this.props.match.params.token)
    .then((response) => {
      this.setState({reviews:response.data})
      console.log(response.data)})
    .catch((error) => console.log(error));
   }
   
  render() {
    const { classes } = this.props;

    return(

      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <Typography variant="title" color="inherit" className={classes.grow}>
              LOGADO
         <Link to={{ pathname: '/review/' + this.state.token + '/' + this.state.user }}><Button variant="contained" color="primary" className={classes.button}>Voltar para reviews</Button></Link>
            </Typography>
          </Toolbar>
        </AppBar>
        <br />
        <center>
          <Typography variant="display5" component="h2">
            REVIEWS
          </Typography>
          {this.state.reviews.map((item) => {
            return (
              <Card className={classes.card} key={item.date}>
                <CardContent>
                  <Typography variant="display2" component="h2">
                    {item.coffeeShopId}
                  </Typography>
                  <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Revisor: {this.state.nome}
                  </Typography>
                  <Typography className={classes.pos} color="textSecondary">
                    Data: {new Date(item.date).getDate() + "/" + new Date(item.date).getMonth() + "/" + new Date(item.date).getFullYear()}
                  </Typography>
                  <Typography component="p">
                    {item.nota} estrelas
                      <br />
                    {item.comentario}
                  </Typography>
                </CardContent>
                <CardActions className={classes.actions} disableActionSpacing>
                  <IconButton onClick={this.handleDelete(item.id)} className={classes.delete} aria-label="Delete"> <DeleteIcon /> </IconButton>
                  <Link to={{ pathname: '/editarreview/' + this.state.token + '/' + this.state.user + '/' + item.id }} >
                    <Button className={classes.button} > Editar </Button>
                  </Link>
                </CardActions>
              </Card>
            )
          })}
        </center>
      </div>
    )
  }
}
RevisorReview.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RevisorReview);