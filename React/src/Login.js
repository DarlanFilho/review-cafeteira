import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { Redirect } from 'react-router'

const axios = require('axios');

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    width: "250px"
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
    root: {
    flexGrow: 1,
  },
});

class Inicio extends React.Component {
  constructor(props){
    super(props);
    this.state={
    email:"",
    password:"",
    showPassword: false,
    redirect: false,
    token:"",
    user:"",
    resposta:""
    }
   }

   handleChange = event => {
    this.setState({ [event.target.id]: event.target.value});
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleClick = () => {
    axios.post('http://localhost:3001/api/revisores/login', {"email": this.state.email, "password": this.state.password})
    .then(response => {
      console.log(response);
      if(response.status == 200){
        console.log(response.data)
      alert('Login efetuado')
      this.setState({ redirect: true, token: response.data.id, user: response.data.userId })
      }
      })
      .catch((error) => {
        console.log(error);
        this.setState({resposta: "Usuário ou senha inválidos"})
      })
      }

  render() {
    const { classes } = this.props;

    if(this.state.redirect){
      return <Redirect to={{pathname: '/review/' + this.state.token + "/" + this.state.user}} />
    }

    return <div className={classes.root}>
      <AppBar position="static">
        <Link to={{ pathname: '/' }}>
          <Button variant="contained" color="primary" className={classes.button}>
            Voltar às reviews
				      </Button>
        </Link>
      </AppBar>
        <center>
        <Typography variant="title" color="inherit">
              Login
        </Typography>      
        <br />
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="name-simple">Email</InputLabel>
          <Input id="email" onChange={this.handleChange} />
        </FormControl>
        <br />
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="name-simple">Senha</InputLabel>
            <Input id="password" type={this.state.showPassword ? "text" : "password"} onChange={this.handleChange} endAdornment={<InputAdornment position="end">
                  <IconButton aria-label="Toggle password visibility" onClick={this.handleClickShowPassword}>
                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>} />
          </FormControl>
          <br /> <br />
          <Button onClick={this.handleClick} variant="contained" color="primary" className={classes.button}>
            Entrar
          </Button>
          <br />
          {this.state.resposta}
          <br /> <br />
          <Typography variant="input" color="inherit">
            Ainda não tem cadastro? Cadastre-se.
          </Typography>
          <br />
          <Link to="/cadastro">
            <Button variant="contained" color="primary" className={classes.button}>
              Cadastro
            </Button>
          </Link>
        </center>
      </div>;
  }
}
Inicio.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Inicio);