import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const axios = require('axios');

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  formControla: {
    margin: theme.spacing.unit,
    width: 400, 
    height: 100
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
    root: {
    flexGrow: 1,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 180,
  },
});

const notas = [
  { value: 1 },{ value: 2 },{ value: 3 },{ value: 4 },{ value: 5 },{ value: 6 },{ value: 7 },{ value: 8 },{ value: 9 },{ value: 10 }
];

class EditarReview extends React.Component {

  constructor(props) {

    super(props)
    this.state = {
      coffeeshops: [],
      coffeeshop: '',
      nota: '',
      comentario: '',
      token: '',
    }
  }


  componentDidMount(){

    axios.get('http://localhost:3001/api/CoffeeShops?access_token=' + this.props.match.params.token )
    .then((response) => {
      this.setState({coffeeshops:response.data})
    })
    .catch((error) => console.log(error));

    axios.get('http://localhost:3001/api/Reviews/' +this.props.match.params.review + '?access_token=' + this.props.match.params.token )
    .then((response) => {
      console.log(response.data)
      this.setState({coffeeshop:response.data.coffeeShopId})
      this.setState({nota:response.data.nota})
      this.setState({comentario:response.data.comentario})
    })
    .catch((error) => console.log(error));
  }

  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  }

  handleCafe = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleClick = event => {

    var datas = new Date(Date.now()).getDate() + "/" + new Date(Date.now()).getMonth() + "/" + new Date(Date.now()).getFullYear();

    axios.put('http://localhost:3001/api/Reviews/' + this.props.match.params.review + '?access_token=' + this.props.match.params.token, {data: datas, date: Date.now(), nota: this.state.nota, comentario: this.state.comentario, coffeeShopId: this.state.coffeeshop, id: this.props.match.params.review, idRevisor: this.props.match.params.id })
    .then(response => {
      if(response.status == 200){
      alert('Review editada')
      }})
    .catch(error => {
       console.log(error.response)
     });
  }

  render() {
    const { classes } = this.props;

    return(

      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <Typography variant="title" color="inherit">
              LOGADO
            <Link to={{ pathname: '/revisorreview/' + this.props.match.params.token + '/' + this.props.match.params.id }}>
                <Button variant="contained" color="primary" className={classes.button}>
                  Voltar às reviews
				      </Button>
              </Link>

            </Typography>
          </Toolbar>
        </AppBar>
        <center>
          <br />
          <Typography variant="display5" component="h2" color="inherit" className={classes.grow}>
            Editar review
          </Typography>
          <br />
          <TextField
            id="coffeeshop"
            select
            label="Selecione o café"
            className={classes.textField}
            value={this.state.coffeeshop}
            onChange={this.handleCafe('coffeeshop')}
            margin="normal"
          >
            {this.state.coffeeshops.map(item => (
              <MenuItem key={item.id} value={item.nome} >
                {item.nome}
              </MenuItem>
            ))}
          </TextField>
          <br />
          <TextField
            id="nota"
            select
            label="Selecione a nota"
            className={classes.textField}
            value={this.state.nota}
            onChange={this.handleCafe('nota')}
            margin="normal"
          >
            {notas.map(item => (
              <MenuItem key={item.value} value={item.value} >
                {item.value}
              </MenuItem>
            ))}
          </TextField>
          <br />
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="text">Comentário </InputLabel>
            <Input className={classes.formControla} id="comentario" value={this.state.comentario} onChange={this.handleChange} />
          </FormControl>
          <br />
          <Button onClick={this.handleClick} variant="contained" color="primary" className={classes.button}>
            Atualizar review
          </Button>
        </center>
      </div>
    )
  }
}
EditarReview.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EditarReview);