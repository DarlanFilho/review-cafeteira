import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link, Redirect } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

const axios = require('axios');

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    width: "250px"
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
    root: {
    flexGrow: 1,
  },
});

class Cadastro extends React.Component {

  constructor(props) {

    super(props)
    this.state = {
      nome: '',
      email: '',
      password:'',
      showPassword: false,
      redirect:''
    }
  }

  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value, resposta: '' });
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleClick = () => {
    axios.post('http://localhost:3001/api/Revisores', {"username": this.state.nome, "email": this.state.email, "password": this.state.password})
    .then(response => {
      console.log(response);
      console.log(response.data);
      if(response.status == 200){
      alert('Usuário cadastrado')
      this.setState({ redirect: true})
      }
      })
    }

  render() {
    const { classes } = this.props;

    if(this.state.redirect){
      return <Redirect to={{pathname: '/login/' }} />
    }

    return(

      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <Typography variant="title" color="inherit">
              <Link to="/login">
                <Button variant="contained" color="primary" className={classes.button}>
                  Voltar ao login
				      </Button>
              </Link>
            </Typography>
          </Toolbar>
        </AppBar>
        <center>
          <br />
          <Typography variant="title" color="inherit" className={classes.grow}>
            Cadastrar novo usuário
           </Typography>
          <br />
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="name-simple">Nome</InputLabel>
            <Input id="nome" onChange={this.handleChange} />
          </FormControl>
          <br />
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="name-simple">Email</InputLabel>
            <Input id="email" onChange={this.handleChange} />
          </FormControl>
          <br />
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="name-simple">Senha</InputLabel>
            <Input
              id="password"
              type={this.state.showPassword ? 'text' : 'password'}
              onChange={this.handleChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword}
                  >
                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <br />
          <Button onClick={this.handleClick} variant="contained" color="primary" className={classes.button}>
            Cadastrar usuário
          </Button>
        </center>
      </div>
    )
  }
}
Cadastro.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Cadastro);
