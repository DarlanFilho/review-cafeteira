import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './Login';
import Review from './Review';
import Cadastro from './Cadastro'
import NovaReview from './NovaReview'
import RevisorReview from './RevisorReview'
import EditarReview from './EditarReview'
import Inicio from './Inicio'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={Inicio} />
            <Route path="/review/:token/:id" component={Review} />
            <Route path="/revisorreview/:token/:id" component={RevisorReview} />
            <Route path="/login" component={Login} />
            <Route path="/novareview/:token/:id" component={NovaReview} />
            <Route path="/editarreview/:token/:id/:review" component={EditarReview} />
            <Route path="/cadastro" component={Cadastro} />
        </Switch>
    </ BrowserRouter>
    , document.getElementById('root'));
// registerServiceWorker();
